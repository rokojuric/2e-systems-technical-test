import React from "react";

function LandingPage() {
  return (
    <h1>
      This is the landing page where the airports and the airlines will be
      displayed;
    </h1>
  );
}

export default LandingPage;
