import React, { useEffect, useState } from "react";
import axios from "axios";

function AirlineById({ match }) {
  useEffect(() => {
    axios
      .get(`http://localhost:5000/airline/${match.params.id}`)
      .then((res) => setAirline(res.data))
      .catch((err) => console.log(err));
  }, []);

  const [airline, setAirline] = useState({});

  return (
    <div>
      <h1>This is the page where the specific airline will be displayed;</h1>
      <a>Airline name is {airline.name}</a>
    </div>
  );
}

export default AirlineById;
