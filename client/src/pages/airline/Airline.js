import React, { Component, useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

function Airline() {
  useEffect(() => {
    axios
      .get("http://localhost:5000/airline")
      .then((res) => {
        setAirlineList(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const [airlineList, setAirlineList] = useState([]);

  return airlineList.map((airline) => (
    <Link to={`/airline/${airline.airline_id}`} key={airline.airline_id}>
      {airline.name}
    </Link>
  ));
}

export default Airline;
