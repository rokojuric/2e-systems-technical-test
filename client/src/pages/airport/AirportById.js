import { useEffect, useState } from "react";
import axios from "axios";

function AirportById({ match }) {
  useEffect(() => {
    axios
      .get(`http://localhost:5000/airport/${match.params.id}`)
      .then((res) => setAirport(res.data))
      .catch((err) => console.log(err));
  }, []);

  const [airport, setAirport] = useState({});

  return (
    <div>
      <h1>This is the page where the specific airport will be displayed;</h1>
      <a>Airport name is {airport.name}</a>
    </div>
  );
}

export default AirportById;
