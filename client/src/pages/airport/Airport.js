import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

function Airport() {
  useEffect(() => {
    axios
      .get("http://localhost:5000/airport")
      .then((res) => {
        setAirportList(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const [airportList, setAirportList] = useState([]);

  return airportList.map((airport) => (
    <Link to={`/airport/${airport.airport_id}`} key={airport.airport_id}>
      {airport.name}
    </Link>
  ));
}

export default Airport;
