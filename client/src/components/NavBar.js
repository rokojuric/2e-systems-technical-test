import { Link } from "react-router-dom";

function NavBar() {
  return (
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link to="/">
        <a class="navbar-brand">Home</a>
      </Link>
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <Link to="/airport">
            <li class="nav-item active">
              <a class="nav-link">
                Airports <span class="sr-only">(current)</span>
              </a>
            </li>
          </Link>
          <Link to="/airline">
            <li class="nav-item active">
              <a class="nav-link">
                Airlines <span class="sr-only">(current)</span>
              </a>
            </li>
          </Link>
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
