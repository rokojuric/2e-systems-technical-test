import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NavBar from "./components/NavBar";
import LandingPage from "./pages/LandingPage";
import Airport from "./pages/airport/Airport";
import AirportById from "./pages/airport/AirportById";
import Airline from "./pages/airline/Airline";
import AirlineById from "./pages/airline/AirlineById";
import WrongRoute from "./pages/WrongRoute";

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/airport" component={Airport} />
        <Route exact path="/airport/:id" component={AirportById} />
        <Route exact path="/airline" component={Airline} />
        <Route exact path="/airline/:id" component={AirlineById} />
        <Route component={WrongRoute} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
