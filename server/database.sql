CREATE DATABASE ProgrammingTask;

//Croatia, Germany, Greece, United Kingdom
CREATE TABLE country(
    country_code CHAR(6) PRIMARY KEY,
    name VARCHAR(50),
);

CREATE TABLE airline(
    airline_id SERIAL PRIMARY KEY,
    name VARCHAR(50),
    country_code CHAR(6) REFERENCES country(country_code),
);

CREATE TABLE airport(
    airport_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    country_code CHAR(6) REFERENCES country(country_code), 
    latitude DECIMAL(8,6),
    longitude DECIMAL(9,6)
);

CREATE TABLE airport_airline(
    airport_id INTEGER references airport(airport_id),
    airline_id INTEGER references airline(airline_id)
);
