const express = require("express");
const airlineRoutes = express.Router();
const airlineController = require("../controllers/AirlineController");

//ACTUAL ROUTE: http://localhost:5000/airline/
//RETURNS list of all airlines
airlineRoutes.get("/", async (request, response) => {
  airlineController.getAllAirlines(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airline/<id>
//RETURNS data of airline with airline_id = <id>
airlineRoutes.get("/:id", async (request, response) => {
  airlineController.getAirlineById(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airline/
//Route for adding new airline
airlineRoutes.post("/", async (request, response) => {
  airlineController.addNewAirline(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airline/<id>
//Route for updating airline with airline_id = <id>
airlineRoutes.put("/:id", async (request, response) => {
  airlineController.updateAirline(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airline/<id>
//Route for deleting airline with airline_id = <id>
airlineRoutes.delete("/:id", async (request, response) => {
  airlineController.deleteAirline(request, response);
});

module.exports = airlineRoutes;
