const express = require("express");
const airportRoutes = express.Router();
const airportController = require("../controllers/AirportController");

//ACTUAL ROUTE: http://localhost:5000/airport/
//RETURNS list of all airports and some of their data
airportRoutes.get("/", async (request, response) => {
  airportController.getAllAirports(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airport/<id>
//RETURNS data of airport with airport_id = <id> and all the airlines it is associated with
airportRoutes.get("/:id", async (request, response) => {
  airportController.getAirportById(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airport/
//Route for adding new airports
airportRoutes.post("/", async (request, response) => {
  airportController.addNewAirport(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airport/<id>
//Route for adding airline to existing airport with airport_id = <id>
airportRoutes.post("/:id", async (request, response) => {
  airportController.addNewAirline(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airport/<id>
//Route for modifying airport data of airport with airport_id = <id>
airportRoutes.put("/:id", async (request, response) => {
  airportController.updateAirport(request, response);
});

//ACTUAL ROUTE: http://localhost:5000/airport/<id>
//Route for deleting airport with airport_id = <id>
airportRoutes.delete("/:id", async (request, response) => {
  airportController.deleteAirport(request, response);
});

module.exports = airportRoutes;
