const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.json());
app.use(cors());

//ROUTES
//Routes that start with /airport are handled by airportRoutes
const airportRoutes = require("./routes/AirportRoutes");
app.use("/airport", airportRoutes);
//Routes that start with /airline are handled by airlineRoutes
const airlineRoutes = require("./routes/AirlineRoutes");
app.use("/airline", airlineRoutes);

app.get("/", (request, response) => {
  response.send("Hello World!");
});

//Server started
app.listen(5000, () => {
  console.log("Backend started and listening on http://localhost:5000");
});
