const pool = require("../database");

airportController = module.exports;

airportController.getAllAirports = async (request, response) => {
  try {
    const allAirports = await pool.query("SELECT * FROM airport");
    return response.json(allAirports.rows);
  } catch (err) {
    return response.send(err);
  }
};

airportController.getAirportById = async (request, response) => {
  try {
    const id = request.params.id;
    const airportById = await pool.query(
      "SELECT * FROM airport WHERE airport_id = $1",
      [id]
    );
    const airportAirlines = await pool.query(
      "SELECT airline_id, name, country_code AS airline_country_code FROM airport_airline NATURAL JOIN airline WHERE airport_id = $1",
      [id]
    );

    const airportDataWithAirlines = {
      airport_id: id,
      name: airportById.rows[0].name,
      country_code: airportById.rows[0].country_code,
      latitude: airportById.rows[0].latitude,
      longitude: airportById.rows[0].longitude,
      airlines: airportAirlines.rows,
    };

    return response.json(airportDataWithAirlines);
  } catch (err) {
    return response.send(err);
  }
};

airportController.addNewAirport = async (request, response) => {
  try {
    const name = request.body.name;
    const country_code = request.body.country_code;
    const location = request.body.location;

    const newAirport = await pool.query(
      "INSERT INTO airport (name, country_code, latitude, longitude) VALUES($1, $2, $3, $4) RETURNING *",
      [name, country_code, location.latitude, location.longitude]
    );
    return response.json(newAirport.rows);
  } catch (err) {
    return response.send(err);
  }
};

airportController.addNewAirline = async (request, response) => {
  try {
    const airport_id = request.params.id;
    const airline_id = request.body.airline_id;

    const newAirportAirline = await pool.query(
      "INSERT INTO airport_airline (airport_id, airline_id) VALUES($1, $2) RETURNING *",
      [airport_id, airline_id]
    );
    return response.json(newAirportAirline.rows[0]);
  } catch (err) {
    return response.send(err);
  }
};

airportController.updateAirport = async (request, response) => {
  try {
    const id = request.params.id;
    const name = request.body.name;
    const country_code = request.body.country_code;
    const location = request.body.location;

    const updateAirport = await pool.query(
      "UPDATE airport SET name = $1 , country_code = $2, latitude = $3, longitude = $4  WHERE airport_id = $5 RETURNING *",
      [name, country_code, location.latitude, location.longitude, id]
    );
    return response.json(updateAirport.rows);
  } catch (err) {
    return response.send(err);
  }
};

airportController.deleteAirport = async (request, response) => {
  try {
    const id = request.params.id;
    const deleteAirport = await pool.query(
      "DELETE FROM airport WHERE airport_id = $1 CASCADE",
      [id]
    );
    response.json("Airport with id " + id + " was deleted");
  } catch (err) {
    return response.send(err);
  }
};
