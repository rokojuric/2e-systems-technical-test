const pool = require("../database");

airlineController = module.exports;

airlineController.getAllAirlines = async (request, response) => {
  try {
    const allAirlines = await pool.query("SELECT * FROM airline");
    return response.json(allAirlines.rows);
  } catch (err) {
    return response.send(err);
  }
};

airlineController.getAirlineById = async (request, response) => {
  try {
    const id = request.params.id;
    const airlineById = await pool.query(
      "SELECT * FROM airline WHERE airline_id = $1",
      [id]
    );
    return response.json(airlineById.rows[0]);
  } catch (err) {
    return response.send(err);
  }
};

airlineController.addNewAirline = async (request, response) => {
  try {
    const name = request.body.name;
    const country_code = request.body.country_code;

    const newAirline = await pool.query(
      "INSERT INTO airline (name,country_code) VALUES($1, $2) RETURNING *",
      [name, country_code]
    );
    return response.json(newAirline.rows);
  } catch (err) {
    return response.send(err);
  }
};

airlineController.updateAirline = async (request, response) => {
  try {
    const id = request.params.id;
    const name = request.body.name;
    const country_code = request.body.country_code;

    const updateAirline = await pool.query(
      "UPDATE airline SET name = $1 , country_code = $2 WHERE airline_id = $3 RETURNING *",
      [name, country_code, id]
    );
    return response.json(updateAirline.rows[0]);
  } catch (err) {
    return response.send(err);
  }
};

airlineController.deleteAirline = async (request, response) => {
  try {
    const id = request.params.id;
    const deleteAirline = await pool.query(
      "DELETE FROM airline WHERE airline_id = $1",
      [id]
    );
    response.json("Airline with id " + id + " was deleted");
  } catch (err) {
    return response.send(err);
  }
};
